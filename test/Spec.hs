{-# LANGUAGE DataKinds, NoImplicitPrelude, TypeFamilies, FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses, FlexibleContexts, OverloadedStrings, GADTs, RankNTypes #-}
import Prelude hiding (id)

import Database.PostgreSQL.Simple

import Data.Basic
import Control.Monad.IO.Class
import Control.Lens hiding ((<.))
import Data.Function ((&))
import Data.String.Conv (toS)
import Model
import Data.FileEmbed
import Data.Text (Text)

-- insertNewSession :: MonadIO m => Connection -> LocalTime -> m ()
-- insertNewSession conn expiration =
--     handleBasicPsql conn query'
--     where
--         session = newUserSession
--             & expirationDate .~ expiration
--             & id .~ 0
--         query'  = void $ insert session


main :: IO ()
main = do
    conn <- connectPostgreSQL "host=localhost port=5432 user=chop_drop dbname=chop_drop password=admin connect_timeout=10"
    handleBasicPsql conn $ do
        -- executeQuery "drop owned by basic_test;" ()
        -- executeQuery (toS $(embedFile "test/initial.sql")) ()
        -- prods <- allProductDetailses &
        --     dfilter (\pd ->
        --         (pd ^. Model.product . Model.name) `Data.Basic.ilike` ("Cheese%" :: Text)
        --         &&. "Grilla" `inPgArray` (pd ^. Model.tags))
        -- liftIO $ print prods
        ords <- allOrders & dfilter (\o -> djust (o ^. Model.relativeDiscount) <. o ^. Model.estimate)
        liftIO $ print ords
        -- newArrtable
        --     & arr .~ PGArray [PGArray [1,2,3], PGArray [1,2,3]]
        --     & narr .~ PGArray [PGArray [1,2,3]]
        --     & insert
        --     & void
        -- allArrtables >>= mapM_ (liftIO . print)

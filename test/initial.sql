-- General tables

CREATE TABLE db_version (
    version integer not null,
    create_date timestamptz not null,
    primary key (version)
);

CREATE TABLE consent_type (
    id UUID not null DEFAULT uuid_generate_v4(),
    name TEXT NOT NULL,
    intent TEXT NOT NULL,
    created timestamptz NOT NULL DEFAULT NOW(),
    expired timestamptz,
    PRIMARY KEY (id)
);

CREATE TABLE consent (
    user_id UUID NOT NULL,
    action_id UUID NOT NULL,
    created timestamptz NOT NULL DEFAULT NOW(),
    valid_to TEXT,
    PRIMARY KEY (user_id, action_id)
);

CREATE TYPE user_role AS ENUM (
    'User', 'Root', 'Admin', 'Driver'
);

CREATE TABLE "user" (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    created timestamptz NOT NULL DEFAULT NOW(),
    role user_role NOT NULL,
    organization_id UUID,
    PRIMARY KEY (id)
);

CREATE TABLE password_login (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    user_id UUID NOT NULL,
    email text NOT NULL,
    password_hash bytea NOT NULL,
    created timestamptz NOT NULL DEFAULT NOW(),
    expired timestamptz,
    verification_code UUID NOT NULL DEFAULT uuid_generate_v4(),
    PRIMARY KEY (id)
);

CREATE TABLE auth_user (
    token text NOT NULL,
    provider text NOT NULL,
    user_id UUID NOT NULL,
    created timestamptz NOT NULL DEFAULT NOW(),
    expiry timestamptz NOT NULL,
    refresh_token text NOT NULL
);

CREATE TABLE notification (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    content json NOT NULL,
    user_id UUID NOT NULL,
    created timestamptz NOT NULL DEFAULT NOW(),
    viewed timestamptz,
    PRIMARY KEY (id)
);


CREATE TABLE "session" (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    user_id UUID NOT NULL,
    created timestamptz NOT NULL DEFAULT NOW(),
    expires timestamptz NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE oauth_request (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    session_id UUID NOT NULL,
    created timestamptz NOT NULL DEFAULT NOW(),
    resolved timestamptz,
    PRIMARY KEY (id)
);

CREATE TABLE password_reset (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    code UUID NOT NULL,
    expires timestamptz NOT NULL,
    used timestamptz,
    password_login_id UUID NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE organization (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    name TEXT NOT NULL,
    invoice_name TEXT NOT NULL,
    created timestamptz NOT NULL DEFAULT NOW(),
    PRIMARY KEY (id)
);


-- user specific tables

CREATE TABLE user_info (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    user_id UUID NOT NULL,
    name text NOT NULL,
    email text NOT NULL,
    PRIMARY KEY (id)
);


CREATE TABLE call (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    organization_id UUID NOT NULL,
    created timestamptz NOT NULL DEFAULT NOW(),
    phone_number TEXT NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE customer (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    organization_id UUID NOT NULL,
    created timestamptz NOT NULL DEFAULT NOW(),
    name TEXT,
    PRIMARY KEY (id)
);

CREATE TABLE address (
    id UUID NOT NULL default uuid_generate_v4(),
    created timestamptz NOT NULL DEFAULT NOW(),
    geo point NOT NULL,
    address TEXT NOT NULL
);

-- Product stuff

CREATE TABLE tag (
    id UUID NOT NULL default uuid_generate_v4(),
    organization_id UUID NOT NULL,
    created timestamptz NOT NULL default NOW(),
    parent_id UUID,
    name TEXT NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE product_tag (
    product_id UUID NOT NULL,
    tag_id UUID NOT NULL,
    created timestamptz NOT NULL DEFAULT NOW(),
    PRIMARY KEY (product_id, tag_id)
);

CREATE TABLE product (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    organization_id UUID NOT NULL,
    name TEXT NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE product_price (
    product_id UUID NOT NULL,
    created timestamptz NOT NULL DEFAULT NOW(),
    valid_to timestamptz,
    price INT NOT NULL
);

CREATE TABLE food_addition (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    organization_id UUID NOT NULL,
    name TEXT NOT NULL,
    created timestamptz DEFAULT NOW(),
    PRIMARY KEY (id)
);

CREATE TABLE food_addition_applied_tag (
    food_addition_id UUID NOT NULL,
    tag_id UUID NOT NULL,
    created timestamptz NOT NULL DEFAULT NOW(),
    PRIMARY KEY (food_addition_id, tag_id)
);

CREATE TABLE "order" (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    organization_id UUID NOT NULL,
    created timestamptz NOT NULL DEFAULT NOW(),
    relative_discount INT NOT NULL,
    absolute_discount INT NOT NULL,
    estimate INT,
    note TEXT,
    address_id UUID NOT NULL,
    call_id UUID,
    delivery_id UUID,
    status TEXT NOT NULL default 'Unassigned',
    PRIMARY KEY (id)
);

CREATE TABLE order_product (
    product_id UUID NOT NULL,
    order_id UUID NOT NULL,
    quantity INT NOT NULL,
    -- dodaci proizvodu
    relative_discount INT NOT NULL,
    absolute_discount INT NOT NULL,
    note TEXT,
    PRIMARY KEY (product_id, order_id)
);

CREATE TABLE delivery (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    organization_id UUID NOT NULL,
    created timestamptz NOT NULL DEFAULT NOW(),
    estimated_time INT NOT NULL,
    real_time INT,
    estimated_distance INT NOT NULL,
    real_distance INT,
    driver_id UUID,
    confirmed timestamptz,
    suggested_route POINT[] NOT NULL,
    PRIMARY KEY (id)
);

-- CREATE TABLE driver_path (
-- );

CREATE TABLE driver_location (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    driver_id UUID NOT NULL,
    location point NOT NULL,
    send timestamptz NOT NULL DEFAULT now(),
    created timestamptz NOT NULL DEFAULT NOW(),
    PRIMARY KEY (id)
);

CREATE TABLE supply_location (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    organization_id UUID NOT NULL,
    location_name text NOT NULL,
    address TEXT NOT NULL,
    geo POINT NOT NULL,
    created timestamptz NOT NULL DEFAULT NOW(),
    PRIMARY KEY (id)
);

CREATE TABLE driver_info (
    user_id UUID NOT NULL,
    base UUID NOT NULL,
    created timestamptz NOT NULL DEFAULT NOW(),
    blocked boolean,
    PRIMARY KEY (user_id)
);

CREATE TABLE invoice (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    organization_id UUID NOT NULL,
    created timestamptz NOT NULL DEFAULT NOW(),
    due timestamptz NOT NULL DEFAULT NOW(),
    paid timestamptz,
    amount INT NOT NULL,
    status TEXT NOT NULL,
    PRIMARY KEY (id)
);

CREATE VIEW product_details (product, price, tags) AS
    SELECT row(product.*) :: product, product_price.price, tags.tag_names
    FROM
        product,
        product_price,
        (
            select product_tag.product_id, array_agg(tag.name) as tag_names
            from product_tag, tag
            where product_tag.tag_id = tag.id
            group by product_tag.product_id
        ) as tags
    WHERE
        product.id = product_price.product_id AND
        product_price.valid_to IS NULL AND
        product.id = tags.product_id;

-- gledati koliko vozacu treba da dodje ponovno u restoran

{-# LANGUAGE TemplateHaskell, DataKinds, TypeFamilies, FlexibleContexts, DeriveGeneric, FlexibleInstances, MultiParamTypeClasses, DerivingStrategies, DeriveAnyClass #-}
{-# OPTIONS_GHC -Wno-orphans #-}
module Model where

import Internal.Interlude hiding (Product)
import Data.Basic
import Internal.Data.Basic.TH.Generator

mkFromFile "test/initial.sql"

return $ schemaValue "initial" "test/initial.sql"

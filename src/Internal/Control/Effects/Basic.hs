{-# LANGUAGE RankNTypes, ConstraintKinds, FlexibleContexts, TypeFamilies, ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications, TypeOperators, DataKinds, FlexibleInstances, UndecidableInstances #-}
{-# LANGUAGE TypeFamilyDependencies, NoMonomorphismRestriction #-}
{-# LANGUAGE GADTs #-}
module Internal.Control.Effects.Basic (module Internal.Control.Effects.Basic, module Control.Effects)
    where

import Internal.Interlude hiding (Error)

import Control.Effects
import Database.PostgreSQL.Simple
import qualified Database.PostgreSQL.Simple.Types as PSQL

import Internal.Data.Basic.Types
import Internal.Data.Basic.Sql.Types
import Internal.Data.Basic.Compiler
import Control.Effects.Logging
import Control.Exception
import Control.Effects.Signal
import Control.Monad.Trans

newtype SqlRequest a = SqlRequest { getSqlRequest :: SqlExp } deriving Show
data Basic m = BasicMethods
    { _runSql :: forall a. FromRow a => SqlRequest a -> m [a]
    , _executeSql :: SqlExp -> m () }

instance Effect Basic where
    liftThrough (BasicMethods r e) = BasicMethods
        (lift . r)
        (lift . e)
    mergeContext m = BasicMethods
        (\a -> ($ a) . _runSql =<< m)
        (\a -> ($ a) . _executeSql =<< m)

runSql :: MonadEffect Basic m => forall a. FromRow a => SqlRequest a -> m [a]
executeSql :: MonadEffect Basic m => SqlExp -> m ()
BasicMethods runSql executeSql = effect

data BasicException =
      BasicFormatError FormatError
    | BasicQueryError QueryError
    | BasicResultError ResultError
    | BasicSqlError SqlError
    deriving (Eq, Show)

-- | Handles SQL by querying a PostgreSQL database. Leaves logs unhandled.
handleBasicPsqlWithLogging ::
    forall m a. (MonadEffects '[Logging, Signal BasicException Query] m, MonadIO m)
    => Connection -> RuntimeImplemented Basic m a -> m a
handleBasicPsqlWithLogging conn = implement $
    BasicMethods (queryOrExec query . sqlExpToQuery . getSqlRequest) (void . queryOrExec execute . sqlExpToQuery)
  where
    queryOrExec :: forall b. (forall q. ToRow q => Connection -> Query -> q -> IO b) -> QuerySegment -> m b
    queryOrExec qe qs@(QuerySegment q as) = do
        logDebug "Executing query"
            & setDataToShowOf qs
            & layerLogs (Context "data-basic")
            & setTimestampToNow
        eitherRes <- liftIO $
            qe conn q as
                & fmap Right
                & handle (\(e :: FormatError) -> return $ Left (BasicFormatError e))
                & handle (\(e :: QueryError) -> return $ Left (BasicQueryError e))
                & handle (\(e :: ResultError) -> return $ Left (BasicResultError e))
                & handle (\(e :: SqlError) -> return $ Left (BasicSqlError e))
        case eitherRes of
            Left e -> do
                logError "Error while executing query" & setDataToShowOf e
                q' <- signal e
                queryOrExec qe (QuerySegment q' [])
            Right res -> return res


throwBasicToIO :: forall m a. MonadIO m => ExceptT BasicException m a -> m a
throwBasicToIO = handleException throwBasicEx
    where
    throwBasicEx :: BasicException -> m a
    throwBasicEx (BasicFormatError fe) = liftIO $ throwIO fe
    throwBasicEx (BasicQueryError fe) = liftIO $ throwIO fe
    throwBasicEx (BasicResultError fe) = liftIO $ throwIO fe
    throwBasicEx (BasicSqlError fe) = liftIO $ throwIO fe

logOnlyErrors :: MonadEffect Logging m => RuntimeImplemented Logging m a -> m a
logOnlyErrors = filterLogs (\l -> originContext l /= Just (Context "data-basic") || logLevel l == Error)

-- | Handles SQL by querying a PostgreSQL database. Writes logs to console.
handleBasicPsql ::
    MonadIO m
    => Connection -> RuntimeImplemented Basic (RuntimeImplemented Logging (ExceptT BasicException m)) a -> m a
handleBasicPsql conn =
      throwBasicToIO
    . prettyPrintSummary 1000
    . handleBasicPsqlWithLogging conn

type family AllTables tables where
    AllTables '[x] = x
    AllTables (x ': xs) = x :. AllTables xs

class (FromRow (AllTables ts)) => AllHaveFromRowInstance ts where
    compositeToTuple :: proxy ts -> AllTables ts -> DbResult ts

instance (FromRow a) => AllHaveFromRowInstance '[a] where
    compositeToTuple _ = Entity

instance (FromRow a, FromRow b) => AllHaveFromRowInstance '[a, b] where
    compositeToTuple _ (a :. b) = (Entity a, Entity b)

instance (FromRow a, FromRow b, FromRow c) => AllHaveFromRowInstance '[a, b, c] where
    compositeToTuple _ (a :. b :. c) = (Entity a, Entity b, Entity c)

instance (FromRow a, FromRow b, FromRow c, FromRow d) => AllHaveFromRowInstance '[a, b, c, d] where
  compositeToTuple _ (a :. b :. c :. d) = (Entity a, Entity b, Entity c, Entity d)

instance (FromRow a, FromRow b, FromRow c, FromRow d, FromRow e) => AllHaveFromRowInstance '[a, b, c, d, e] where
  compositeToTuple _ (a :. b :. c :. d :. e) = (Entity a, Entity b, Entity c, Entity d, Entity e)

instance (FromRow a, FromRow b, FromRow c, FromRow d, FromRow e, FromRow f) => AllHaveFromRowInstance '[a, b, c, d, e, f] where
  compositeToTuple _ (a :. b :. c :. d :. e :. f) = (Entity a, Entity b, Entity c, Entity d, Entity e, Entity f)

instance (FromRow a, FromRow b, FromRow c, FromRow d, FromRow e, FromRow f, FromRow g) => AllHaveFromRowInstance '[a, b, c, d, e, f, g] where
  compositeToTuple _ (a :. b :. c :. d :. e :. f :. g) = (Entity a, Entity b, Entity c, Entity d, Entity e, Entity f, Entity g)

instance (FromRow a, FromRow b, FromRow c, FromRow d, FromRow e, FromRow f, FromRow g, FromRow h) => AllHaveFromRowInstance '[a, b, c, d, e, f, g, h] where
  compositeToTuple _ (a :. b :. c :. d :. e :. f :. g :. h) = (Entity a, Entity b, Entity c, Entity d, Entity e, Entity f, Entity g, Entity h)

instance (FromRow a, FromRow b, FromRow c, FromRow d, FromRow e, FromRow f, FromRow g, FromRow h, FromRow i) => AllHaveFromRowInstance '[a, b, c, d, e, f, g, h, i] where
  compositeToTuple _ (a :. b :. c :. d :. e :. f :. g :. h :. i) = (Entity a, Entity b, Entity c, Entity d, Entity e, Entity f, Entity g, Entity h, Entity i)

instance (FromRow a, FromRow b, FromRow c, FromRow d, FromRow e, FromRow f, FromRow g, FromRow h, FromRow i, FromRow j) => AllHaveFromRowInstance '[a, b, c, d, e, f, g, h, i, j] where
  compositeToTuple _ (a :. b :. c :. d :. e :. f :. g :. h :. i :. j) = (Entity a, Entity b, Entity c, Entity d, Entity e, Entity f, Entity g, Entity h, Entity i, Entity j)


runDbStatement :: forall ts m f.
            (AllHaveFromRowInstance ts, MonadEffect Basic m)
         => DbStatement f ts -> m [DbResult ts]
runDbStatement = fmap (map (compositeToTuple (Proxy @ts)))
         . runSql
         . SqlRequest
         . compileToSql

executeDbStatement :: MonadEffect Basic m =>  DbStatement f ts -> m ()
executeDbStatement = executeSql . compileToSql

type family WithoutOnly a where
    WithoutOnly (PSQL.Only a) = a
    WithoutOnly (PSQL.PGArray a) = [a]
    WithoutOnly (a, b) =             (WithoutOnly a, WithoutOnly b)
    WithoutOnly (a, b, c) =          (WithoutOnly a, WithoutOnly b, WithoutOnly c)
    WithoutOnly (a, b, c, d) =       (WithoutOnly a, WithoutOnly b, WithoutOnly c, WithoutOnly d)
    WithoutOnly (a, b, c, d, e) =    (WithoutOnly a, WithoutOnly b, WithoutOnly c, WithoutOnly d, WithoutOnly e)
    WithoutOnly (a, b, c, d, e, f) = (WithoutOnly a, WithoutOnly b, WithoutOnly c, WithoutOnly d, WithoutOnly e, WithoutOnly f)
    WithoutOnly [a] = [WithoutOnly a]
    WithoutOnly a = a

type NoOnly a = Coercible a (WithoutOnly a)
noOnly :: NoOnly a => a -> WithoutOnly a
noOnly = coerce

runAggregateStatement ::
    forall aggr m.
    ( MonadEffect Basic m
    , FromRow (AggregationResult aggr)
    , NoOnly (AggregationResult aggr))
    => AggregateStatement aggr 'AM -> m (WithoutOnly (AggregationResult aggr))
runAggregateStatement =
    fmap (
        noOnly @(AggregationResult aggr)
        . unsafeHead)
    . runSql
    . SqlRequest
    . aggregateStatementToSql

runMapStatement ::
    forall res m f.
    ( MonadEffect Basic m
    , FromRow res
    , NoOnly res )
    => DbStatement f '[res] -> m [WithoutOnly res]
runMapStatement =
      fmap (noOnly @[res])
    . runSql
    . SqlRequest
    . compileToSql

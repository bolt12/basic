{-# OPTIONS_GHC -fno-warn-name-shadowing #-}
{-# LANGUAGE AllowAmbiguousTypes        #-}
{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE DuplicateRecordFields      #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE InstanceSigs               #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE PolyKinds                  #-}
{-# LANGUAGE Rank2Types                 #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE TypeOperators              #-}
{-# LANGUAGE TypeApplications           #-}
{-# LANGUAGE UndecidableInstances       #-}
{-# LANGUAGE UndecidableSuperClasses    #-}
module Internal.Data.Basic.TH (mkFromFile, mkFromDatabase, printToFile) where

import           Internal.Interlude            hiding (Type)
import           System.Process                (readProcess)
import           Language.Haskell.TH           hiding (Name)
import qualified Language.Haskell.TH.Syntax    as TH
import qualified Data.Schema.File              as P
import Data.Schema.Types.Lens

import qualified Internal.Data.Basic.TH.Generator as G
import qualified Control.Lens.Internal.FieldTH as LTHI

-- | Generates haskell code from an SQL file.
mkFromFile :: FilePath -> Q [Dec]
mkFromFile filename = do
  TH.addDependentFile filename
  res <- runIO $ P.semanticParseFile filename
  case res of
    Left (P.SemanticError err) ->
      error (show err)
    Left (P.ParseError err) ->
      error (toS err)
    Right (_, schema) -> runQ $ do
      enums <- G.generateCustomTypes schema
      decs <- mapM (G.dataConstructor' schema) (schema ^. tables)
      let result = G.compileEntity' schema <$> schema ^. tables
      let fieldOptics = G.fieldOptics'  schema
      lenses <- concat <$> sequence (LTHI.makeFieldOpticsForDec lensRules <$> decs)
      tableFields <- concat <$> mapM (G.tableFields' schema) (schema ^. tables)
      return $ enums <> decs <> concat result <> lenses <> fieldOptics <> tableFields

mkFromDatabase :: Text -> Q [Dec]
mkFromDatabase connectionString = do
  output <- runIO $ readProcess "pg_dump" ["--schema-only", "postgres://" <> toS connectionString] []
  res <- runIO $ P.semanticParse $ toS output
  case res of
    Left (P.SemanticError err) ->
      error (show err)
    Left (P.ParseError err) ->
      error (toS err)
    Right (_, schema) -> runQ $ do
      enums <- G.generateCustomTypes schema
      decs <- mapM (G.dataConstructor' schema) (schema ^. tables)
      let result = G.compileEntity' schema <$> schema ^. tables
      let fieldOptics = G.fieldOptics'  schema
      lenses <- concat <$> sequence (LTHI.makeFieldOpticsForDec lensRules <$> decs)
      tableFields <- concat <$> mapM (G.tableFields' schema) (schema ^. tables)
      return $ enums <> decs <> concat result <> lenses <> fieldOptics <> tableFields

-- | Allows you to print generated template haskell code to a file
printToFile :: FilePath -> FilePath -> Q [Dec]
printToFile filename filenameOut = do
  TH.addDependentFile filename
  res <- runIO $ P.semanticParseFile filename
  case res of
    Left (P.SemanticError err) ->
      error (show err)
    Left (P.ParseError err) ->
      error (toS err)
    Right (_, schema) -> runQ $ do
      enums <- G.generateCustomTypes schema
      decs <- mapM (G.dataConstructor' schema) (schema ^. tables)
      let result = G.compileEntity' schema <$> schema ^. tables
      let fieldOptics = G.fieldOptics'  schema
      lenses <- concat <$> sequence (LTHI.makeFieldOpticsForDec lensRules <$> decs)
      tableFields <- concat <$> mapM (G.tableFields' schema) (schema ^. tables)
      let r = enums <> decs <> concat result <> lenses <> fieldOptics <> tableFields
      let out = concatMap (<> "\n\n") $ pprint <$> r
      runIO $ writeFile filenameOut $ toS out
      return r

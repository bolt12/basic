module Internal.Data.Basic.AsDbEntity where

import Internal.Interlude
import Internal.Data.Basic.Replace
import Internal.Data.Basic.Types
import Internal.Control.Effects.Basic
import Internal.Data.Basic.Common

asDbEntity :: forall table pk fields d m.
      ( Table table
      , 'Just pk ~ TablePrimaryKey table
      , fields ~ UniqueFields pk
      , PrimaryKeyMatch fields table
      , MonadEffect Basic m )
     => Entity ('Fresh '[]) table -> m (Maybe (Entity ('FromDb d) table))
asDbEntity ent = do
    rows <- allRows @(TableName table)
        & dfilter (primaryKeyMatch @fields ent)
    case rows of
        [_] -> return (Just (coerce ent))
        _ -> return Nothing
{-|
module Internal.     : Data.Basic.TH.Types
Description : Data types and utility function used during TH generation phase
Copyright   : (c) Nikola Henezi, 2016-2017
                  Luka Horvat, 2016-2017
License     : MIT

This module Internal.defines data types that are used during parsing and TH generation stages. They describe
structure of the parser and define minimal representation that is needed from parser in order to
generate Template Haskell.


-}
module Internal.Data.Basic.TH.Types where

import Internal.Interlude hiding (Type)

import           Data.Time
import           Data.Scientific
import           Data.UUID
import           Internal.Data.Basic.SqlToHsTypes
import           Database.PostgreSQL.Simple.Types (PGArray(..))

-- | Defines null values for all datatypes that we support in Basic.
-- This is basically a hack to avoid usage of unsafeCoerce, because with unsafeCoerce
-- you can hit segmentation faults due to wrong memory allocation (e.g. 'Int' and 'Bool').
class NullValue a where
    nullValue :: a

instance NullValue Int where nullValue = 0
instance NullValue Bool where nullValue = False
instance NullValue LocalTime where nullValue = LocalTime (ModifiedJulianDay 0) nullValue
instance NullValue ZonedTime where nullValue = ZonedTime nullValue (TimeZone 0 False "")
instance NullValue TimeOfDay where nullValue = TimeOfDay 0 0 0
instance (NullValue a, NullValue b) => NullValue (a, b) where nullValue = (nullValue, nullValue)
instance NullValue Double where nullValue = 0
instance NullValue Text where nullValue = ""
instance NullValue Scientific where nullValue = 0
instance NullValue Day where nullValue = ModifiedJulianDay 0
instance NullValue ByteString where nullValue = ""
instance NullValue (Maybe a) where nullValue = Nothing
instance NullValue UUID where nullValue = nil
instance NullValue Value where nullValue = Null
instance NullValue Point where nullValue = Point 0 0
instance NullValue (PGArray a) where nullValue = PGArray []
newtype Schema = Schema Text

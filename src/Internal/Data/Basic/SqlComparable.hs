{-# LANGUAGE AllowAmbiguousTypes #-}
module Internal.Data.Basic.SqlComparable where

import Internal.Interlude
import Internal.Data.Basic.Sql.Types

class SqlComparable (eqOrOrd :: * -> Constraint) a where
    sqlCompare :: Comparison -> SqlValueExp -> SqlValueExp -> Condition

instance {-# OVERLAPPABLE #-} Eq a => SqlComparable Eq a where
    sqlCompare = SqlOperator

instance {-# OVERLAPPABLE #-} Ord a => SqlComparable Ord a where
    sqlCompare = SqlOperator

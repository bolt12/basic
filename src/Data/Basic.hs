{-# LANGUAGE StandaloneDeriving, GeneralizedNewtypeDeriving #-}
{-# OPTIONS_GHC -Wno-orphans #-}
module Data.Basic
    ( Key, Point(..), Table(..), TableField(..), UniqueConstraint(..), PrimaryKeyConstraint
    , ForeignKeyConstraint(..), MissingField(..), FieldConstraint(..), AllRows, Entity(..)
    , EntityKind(..), VirtualTable, virtualTableLens, Getter', FieldOpticProxy, fieldOptic
    , ForeignKeyLensProxy, foreignKeyLens
    , MonadEffect, Basic, allRows
    , ddelete, dupdate, insert, dfilter, save, dtake, ddrop, djoin, dsortOn, dfoldMap, dfoldMapInner, dmap
    , dgroupOn, rawQuery
    , (<.), (>.), (==.), (/=.), (<=.), (>=.), (&&.), (||.)
    , ConditionExp(In), Avg(..), Count(..), Min(..), Max(..), Sum(..), List(..), PGArray(..)
    , handleBasicPsql, connectPostgreSQL, handleBasicPsqlWithLogging
    , BasicException(..), throwBasicToIO, logOnlyErrors, prettyPrintSummary
    , mkFromFile, mkFromDatabase, printToFile, FromRow(..), field, Cached(..)
    , delem, disNothing, disJust, GettableField, ModifyableField, SettableField
    , WithFieldSet, like, ilike, dtrue, dfalse, executeQuery, applySchema, Schema, toFreshEntity
    , Only(..), inPgArray, djust, dnot, asDbEntity )
    where

import Internal.Data.Basic.Types
import Internal.Data.Basic
import Internal.Data.Basic.TH
import Internal.Data.Basic.TH.Types (Schema)
import Internal.Control.Effects.Basic
import Internal.Data.Basic.SqlToHsTypes
import Database.PostgreSQL.Simple
import Database.PostgreSQL.Simple.FromRow
import Database.PostgreSQL.Simple.Types
import Control.Effects.Logging
import Internal.Data.Basic.AsDbEntity

import Data.Aeson (FromJSON, ToJSON)
deriving instance FromJSON a => FromJSON (PGArray a)
deriving instance ToJSON a => ToJSON (PGArray a)
